# Kibot example with Gitlab Runner based on Kicad Demo Project stickhub

Two different output, once a simple assembly drawing output with usb4 and d15 not assembled,
and once a rather big output with the kibot option ["--quick-start" ](https://kibot.readthedocs.io/en/latest/configuration/quick_start.html)

[.gitlab-ci.yml](https://gitlab.com/stefan_test/kicad_kibot_dnp_snippets/-/blob/main/.gitlab-ci.yml?ref_type=heads) defines the gitlab-runner:

- docker image:

```
image:
  name: ghcr.io/inti-cmnb/kicad8_auto_full:latest
``` 
is a docker container which includes already all needed tools to run kicad and kibot, it's provided by https://github.com/INTI-CMNB.

- kibot command:

```
kibot --log assembly_drawing/kibot.log -c Assembly_DNP.kibot.yaml -b stickhub/stickhub.kicad_pcb
```

writes the log to a specific file, adds a .yaml file with more commands, specifies the to be used kicad_pcb file

[Assembly_DNP.kibot.yaml](https://gitlab.com/stefan_test/kicad_kibot_dnp_snippets/-/blob/main/Assembly_DNP.kibot.yaml?ref_type=heads) defines the kibot options for the assembly plan:

```
dnf_filter: _kibom_dnf_Config
```

is the key line, this uses an internal list to filter components with various "dnp" keys, see [Built-in filters](https://kibot.readthedocs.io/en/master/configuration/filters.html#built-in-filters).

##  assembly_drawing output
https://gitlab.com/stefan_test/kicad_kibot_dnp_snippets/-/jobs/6915300376/artifacts/download

## --quick-start output
https://gitlab.com/stefan_test/kicad_kibot_dnp_snippets/-/jobs/6915300377/artifacts/download